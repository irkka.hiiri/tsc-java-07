package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    public static final Command ABOUT = new Command(CommandConst.CMD_ABOUT,
            "show developer info",
            ArgumentConst.ARG_ABOUT);

    public static final Command HELP = new Command(CommandConst.CMD_HELP,
            "show this message",
            ArgumentConst.ARG_HELP);

    public static final Command VERSION = new Command(CommandConst.CMD_VERSION,
            "show version info",
            ArgumentConst.ARG_VERSION);

    public static final Command EXIT = new Command(CommandConst.CMD_EXIT,
            "quit");

    public static final Command INFO = new Command(CommandConst.CMD_INFO,
            "show system info",
            ArgumentConst.ARG_INFO);

    public Command(String name, String description, String arg) {
        this.name = name;
        this.description = description;
        this.arg = arg;
    }

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return name + (arg.isEmpty() ? " - " : " [" + arg + "] - ") + description + ".";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
