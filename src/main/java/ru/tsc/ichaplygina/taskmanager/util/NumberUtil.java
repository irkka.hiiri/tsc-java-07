package ru.tsc.ichaplygina.taskmanager.util;

public class NumberUtil {

    public static String bytesToHumanReadable(final long bytes) {
        final String[] units = {"B","KB","MB","GB","TB"};
        for (int i = 0; i < 5; i++) {
            if (bytes < Math.pow(1024, i+1))
                return (bytes / (long)Math.pow(1024, i)) + " " + units[i];
        }
        return bytes + " " + units[0];
    }

}
