package ru.tsc.ichaplygina.taskmanager;

import ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst;
import ru.tsc.ichaplygina.taskmanager.constant.CommandConst;
import ru.tsc.ichaplygina.taskmanager.model.Command;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import java.util.Scanner;

public class TaskManager {

    public static void main(final String[] args) {
        if (args == null || args.length == 0) processInput();
        else executeCommand(args);
    }

    public static void processInput() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = readCommand(scanner);
        while (true) {
            executeCommand(command);
            command = readCommand(scanner);
        }
    }

    public static void executeCommand(final String command) {
        if (command.isEmpty()) return;
        switch (command) {
            case CommandConst.CMD_ABOUT:
                showAbout();
                break;
            case CommandConst.CMD_VERSION:
                showVersion();
                break;
            case CommandConst.CMD_HELP:
                showHelp();
                break;
            case CommandConst.CMD_INFO:
                showSystemInfo();
                break;
            case CommandConst.CMD_EXIT:
                System.exit(0);
            default:
                showUnknown(command);
        }
    }

    public static void executeCommand(final String[] params) {
        if (params == null || params.length == 0) return;
        switch (params[0]) {
            case ArgumentConst.ARG_ABOUT:
                showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                showSystemInfo();
                break;
            default:
                showUnknown(params[0]);
        }
    }

    public static String readCommand(final Scanner scanner) {
        showCommandPrompt();
        return scanner.nextLine().trim();
    }

    public static void showWelcome() {
        System.out.println("\n*** WELCOME TO THE ULTIMATE TASK MANAGER ***\n");
        System.out.println("Enter help to show available commands.");
        System.out.println("Enter exit to quit.");
        System.out.println("Enter command:\n");
    }

    public static void showCommandPrompt() {
        System.out.print("> ");
    }

    public static void showVersion() {
        System.out.println();
        System.out.println("Version: 0.2.0");
        System.out.println();
    }

    public static void showAbout() {
        System.out.println();
        System.out.println("Developed by:");
        System.out.println("Irina Chaplygina,");
        System.out.println("Technoserv Consulting,");
        System.out.println("ichaplygina@tsconsulting.com");
        System.out.println();
    }

    public static void showHelp() {
        System.out.println();
        System.out.println("Run with command-line arguments (as listed in [brackets]).");
        System.out.println("Run with no arguments to enter command-line mode.");
        System.out.println("Available commands and arguments:");
        System.out.println(Command.VERSION.toString());
        System.out.println(Command.ABOUT.toString());
        System.out.println(Command.HELP.toString());
        System.out.println(Command.INFO.toString());
        System.out.println(Command.EXIT.toString());
        System.out.println();
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int numberOfCpus = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println();
        System.out.println("Available processors: " + numberOfCpus);
        System.out.println("Free memory: " + NumberUtil.bytesToHumanReadable(freeMemory));
        System.out.println("Maximum memory: " + NumberUtil.bytesToHumanReadable(maxMemory));
        System.out.println("Total memory available to JVM: " + NumberUtil.bytesToHumanReadable(totalMemory));
        System.out.println("Memory used by JVM: " + NumberUtil.bytesToHumanReadable(usedMemory));
        System.out.println();
    }

    public static void showUnknown(final String arg) {
        System.out.println();
        System.out.println("Unknown argument " + arg);
        System.out.println();
    }

}
